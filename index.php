<?php
/**
 * Storm 
 * Darren Hamilton ( Darren.S.Hamilton@gmail.com )
 */

   // Reserved for DB Connection Information
   include_once('config.php');
   
   // Let the Class Loader automatically find classes
   include_once('vendor' . DIRECTORY_SEPARATOR .'classloader' . DIRECTORY_SEPARATOR . 'YAClassLoader.class.php'); 
   
   SESSION_START();
   
   Router::loadPage();