<?php

/*
	Below is the example format of the controller we would
	expect to be seeing. The functions/base controller is all psuedo
	however you will require some form of function that handles the 
	rendering of the view.
*/

class ExampleController extends BaseController {
	public function view(){

      $fb = new Facebook\Facebook([
        'app_id' => FACEBOOK_APP_ID,
        'app_secret' => FACEBOOK_SECRET,
        'default_graph_version' => 'v2.5',
      ]);
      
      $helper = $fb->getRedirectLoginHelper();

      $permissions = ['email']; // Optional permissions
      $loginUrl = $helper->getLoginUrl('http://storm.test/example/login', $permissions);

   
		$this->render('app' . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'example_view.php', Array('url' => htmlspecialchars($loginUrl)));
	}
   
   public function login() {

      $fb = new Facebook\Facebook([
        'app_id' => FACEBOOK_APP_ID,
        'app_secret' => FACEBOOK_SECRET,
        'default_graph_version' => 'v2.5',
      ]);
      
      $helper = $fb->getRedirectLoginHelper();
      
      // In case of error, redirect to login for now
      try {
         $accessToken = $helper->getAccessToken();
      } catch(Facebook\Exceptions\FacebookResponseException $e) {
         $this->view();
         exit;
      } catch(Facebook\Exceptions\FacebookSDKException $e) {
         $this->view();
         exit;
      }

      if (! isset($accessToken)) {
         if ($helper->getError()) {
             header('HTTP/1.0 401 Unauthorized');
             echo "Error: " . $helper->getError() . "\n";
             echo "Error Code: " . $helper->getErrorCode() . "\n";
             echo "Error Reason: " . $helper->getErrorReason() . "\n";
             echo "Error Description: " . $helper->getErrorDescription() . "\n";
         } else {
             header('HTTP/1.0 400 Bad Request');
             echo 'Bad request';
         }

         exit;
      }

      // Logged in

      // The OAuth 2.0 client handler helps us manage access tokens
      $oAuth2Client = $fb->getOAuth2Client();

      // Get the access token metadata from /debug_token
      $tokenMetadata = $oAuth2Client->debugToken($accessToken);
      // Validation (these will throw FacebookSDKException's when they fail)
      $tokenMetadata->validateAppId(FACEBOOK_APP_ID);
      // If you know the user ID this access token belongs to, you can validate it here
      //$tokenMetadata->validateUserId('123');
      $tokenMetadata->validateExpiration();

      try {
        // Returns a `Facebook\FacebookResponse` object
        $response = $fb->get('/me?fields=id,name', $accessToken);
      } catch(Facebook\Exceptions\FacebookResponseException $e) {
        echo 'Graph returned an error: ' . $e->getMessage();
        exit;
      } catch(Facebook\Exceptions\FacebookSDKException $e) {
        echo 'Facebook SDK returned an error: ' . $e->getMessage();
        exit;
      }
      
      $facebookUser = $response->getGraphUser();
      $name = $facebookUser['name'];
      
      $user = new User(Array('name' => $name));
      $user->save();
      
      $previousUsers = array_reverse(User::getAll());
      
      $_SESSION['fb_access_token'] = (string) $accessToken;   
      
      
   
   	$this->render('app' . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'example_login.php', Array('name' => $name, 'previousUsers' => $previousUsers));
   }
}

?>