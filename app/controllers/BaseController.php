<?php

/**
 * BaseController - classes inheriting from the BaseController class must define their
 * own view method.
 */

abstract class BaseController {

	public abstract function view();
   
   public function render($filename, $data = Array()) {
      include_once($filename);
   }
}

?>