<?php

class User extends Database {

   public function __construct($data = Array(), $table = 'user') {
      parent::__construct($data, $table);
   }
   
   public static function getAll($table = 'user') {
      return parent::getAll($table);
   }
}