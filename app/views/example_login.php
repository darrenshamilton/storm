<!DOCTYPE HTML>
<html lang="en-GB">
<head>
	<meta charset="UTF-8">
	<title>Storm Test | Thank You For Logging In!</title>
   <link rel="stylesheet" href="../stylesheets/main.css"/>
</head>
<body>
   <h1>Hello <?php echo($data['name']); ?>!</h1>
   <h2>Thank you for logging in!</h2>
   
   <p>The following people logged in before you:</p>
   
   <table>
      <tr>
         <th>Row</th>
         <th>Login Time</th>
         <th>Name</th>
      </tr>
      <?php foreach($data['previousUsers'] as $rowCount => $user) { ?>
         <tr>
            <td><?php echo($rowCount + 1); ?></td>
            <td><?php echo(date('M j Y g:i A', strtotime($user->created))); ?></td>
            <td><?php echo($user->name); ?></td>
         </tr>
      <?php } ?>
   </table>
   
</body>
</html>