<?php

class Database {
   public $data;
   protected $table;

   public function __construct($data = Array(), $table) {
      $this->data = $data;
      $this->table = $table;
   }
   
   /**
    * Magic Methods for __get and __set
    */
   public function __get($name) {
   
      if(array_key_exists($name, $this->data)) {
         return $this->data[$name];
      }
      
      return null;
   }

   public function __set($name, $value) {
      $this->data[$name] = $value;
   }

   public function save() {
      $table = $this->table;
      $data = $this->data;
      
      $dbh = new PDO('mysql:host='. STORM_DB_HOSTNAME . ';dbname=' . STORM_DB_DATABASE, STORM_DB_USERNAME, STORM_DB_PASSWORD);
      $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);

      $id = $this->id;
      
      if(is_null($id)) {
         $id = 0;
      }
   
      if($id == 0) {     
         $fields = '';
         $markedUpFields = '';
         
            foreach($data as $label => $values) {
            $fields .= $label . ', ';
            $markedUpFields .= ':' . $label . ', ';
            }
            
         $fields = substr($fields, 0, -2);
         $markedUpFields = substr($markedUpFields, 0, -2);
         
         $stmt = $dbh->prepare('insert into ' . $table . ' (' . $fields . ') values (' . $markedUpFields . ')');
      } else {
         $sql = '';
         
            foreach($data as $label => $values) {
            $sql .= $label . ' = :' . $label . ', ';
            }
            
         $sql = substr($sql, 0, -2);
         $sql = 'update ' . $table . ' set ' . $sql . ' where id = :id';
         
         $stmt = $dbh->prepare($sql);
      }

      Database::PDOBindArray($stmt,$data);
        
      $stmt->execute();
   
      if($id == 0) {
         $this->id = ((int)$dbh->lastInsertId());
      }

      $dbh = null;
   }
   
   static function PDOBindArray(&$stmt, &$array){

      foreach($array as $k=>$v) {

         $stmt->bindValue(':'.$k,$v);
      }
   }

   public static function getAll($table) {
      $Class = get_called_class();

      $dbh = new PDO('mysql:host='. STORM_DB_HOSTNAME . ';dbname=' . STORM_DB_DATABASE, STORM_DB_USERNAME, STORM_DB_PASSWORD);
      
      $objects = Array();
      $count = 0;

      $stmt = $dbh->prepare('select * from ' . $table);
   
      if ($stmt->execute()) {
         while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $objects[$count] = new $Class($row, $table);
            $count ++;
         }
      }

      $dbh = null;
      return $objects;   
   }
}