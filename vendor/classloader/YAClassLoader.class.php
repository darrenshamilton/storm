﻿<?php
/**
 * Yet Another Class Loader
 * Author: Darren Hamilton
 */
class YAClassLoader {

   static $YAClassLoader;
   
   public $foundClasses;
   
   public $filename = '';
   public $folder = '';
   
   static $lastFoundClasses = Array();

   public function __construct() {
      $this->filename = 'YAClassLoader.obj.php';
      $this->folder = $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'obj';

      if(file_exists($this->folder . DIRECTORY_SEPARATOR . $this->filename)) {
         $fc = file_get_contents($this->folder . DIRECTORY_SEPARATOR . $this->filename);
         $obj = unserialize($fc);
         $this->foundClasses = $obj->foundClasses;
      } else {
         $this->foundClasses = Array();
      }
      
      YAClassLoader::$lastFoundClasses = $this->foundClasses;
   }
   
   public function __destruct() {

      if(count(array_diff($this->foundClasses, YAClassLoader::$lastFoundClasses)) != 0) {
         $fc = serialize($this);
         
         if(!is_dir($this->folder)) {
            mkdir($this->folder);
         }
         
         file_put_contents($this->folder . DIRECTORY_SEPARATOR . $this->filename, $fc);
      }
   }
   
  
}

   /**
    * Auto Load is automatically called when a class is accessed that has not yet been declared.
    * This looks for the class by iterating over all directories within the app and vendors folders until it is found.
    * For a class to be found by this function, the file must be in the following format:
    * {__CLASS_NAME__}.class.php or {__CLASS_NAME__}.php 
    */
   function __autoload($class_name) {

      // Handle namespaces for Facebook
      // Because of the app size conflicts will not be an issue however so we can discard the namespace
      // however, for anything larger this information should considered when deciding which folders to search.
      // Since the found classes are cached by the deconstructor this won't negatively affect performance after the first visitor.
      if(strpos($class_name, '\\') > 0) {
         $parts = explode('\\', $class_name); 
         $class_name = $parts[count($parts) - 1];
      }

   
      if(isset(YAClassLoader::$YAClassLoader->foundClasses[$class_name . '.class.php'])) {
         include(YAClassLoader::$YAClassLoader->foundClasses[$class_name . '.class.php']);
         return true;
      }
      
      if(isset(YAClassLoader::$YAClassLoader->foundClasses[$class_name . '.php'])) {
         include(YAClassLoader::$YAClassLoader->foundClasses[$class_name . '.php']);
         return true;
      }
      
      if(scan($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'app', $class_name . '.class.php') == false) {
         if(scan($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'app', $class_name . '.php') == false) {
            if(scan($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'vendor', $class_name . '.class.php') == false) {
               if(scan($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'vendor', $class_name . '.php') == false) {
                  return false;
               }
            }
         }
         
         return true;
      }
      
      return true;
   }
   
   function scan($folder, $class) {
   
      $dirs = scandir($folder);

      foreach($dirs as $row => $file) {

         if($file != '.' && $file != '..') {
            if(is_dir($folder . '/' . $file)) {
           
               // If the current file is a folder, iterate over it. If it returns true the class has been found,               
               if(scan($folder . '/' . $file, $class) === true) {
                  return true;
               }

            } else {

               // If the name of the current file matches the name of the class then a match has been found and can be included.
               if($file == $class) {
                  include_once($folder . '/' . $file);
                  YAClassLoader::$YAClassLoader->foundClasses[$file] = $folder . '/' . $file;
                  return true;
               }
            }
         }
      }
   }
   
   ini_set('unserialize_callback_func', '__autoload');
   
   YAClassLoader::$YAClassLoader = new YAClassLoader();