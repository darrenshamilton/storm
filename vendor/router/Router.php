<?php

/**
 * The Router will attempt to automatically load the controllers from the URL,
 * in a larger app it would make more sense to have each individual controller register
 * the URL they want to handle.
 * Note: The class name must start with capital letter and the class name must end with "Controller".
 */

class Router {
   
   public static function loadPage() {
         
      $url = explode('/', substr(strtok($_SERVER["REQUEST_URI"],'?'), 1)); // First Element in the REQUEST_URI is a forward slash, skip it, ignore query string too
            
         if(count($url) == 1) { // Controller specified but not view, default to view
            $url[1] = 'view';
         }

         if(count($url) == 2) {
            $controllerClassName = ucfirst($url[0]) . 'Controller';
            
            if(get_parent_class($controllerClassName) == 'BaseController') { // make sure users do not try to execute random classes!
               $controller = new $controllerClassName();
               
               if(method_exists($controller, $url[1])) { // requested view exists?
                  $controller->$url[1]();
                  return; // success - anything that doesn't make it here will hit the 404 error
               }
            }
         }

      /**
       * If the page cannot be found, output a 404 error. 
       */
      header("HTTP/1.0 404 Not Found");
      echo "<h1>404 Not Found</h1>";
      echo "The page that you have requested could not be found.";      
      die();
   }
}

?>